package java.util.function;

@FunctionalInterface
public interface IntBinaryOperator{
    /*
    * IntBinaryOperator io = (x,y)->x +y;
    * System.out.println(io.applyAsInt(2,3));
    * ��� 5
    * */
    int applyAsInt(int left,int right);
}
