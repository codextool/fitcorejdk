package java.util.function;

@FunctionalInterface
public interface DoubleToLongFunction{
    /*
    *  DoubleToLongFunction dl = (x) -> {return Long.MAX_VALUE - (long)x;};
    *  System.out.println(dl.applyAsLong(3.14));
    *  ��� 9223372036854775804
    */
    long applyAsLong(double value);
}
