package java.util.function;

import java.util.Objects;

/*
* Function<Integer,String> converter = (i)-> Integer.toString(i);
* System.out.println(converter.apply(3).length());
* System.out.println(converter.apply(30).length());
* 结果
*       1
*       2
*
* 实际处理逻辑体
* public static String calc(Function<Integer, String> bi, Integer i) {
*       return bi.apply(i);
* }
* 这里是调用的实例
* 作为 lambda 表达式 (a) -> "Result: " + (a * 2), 10，它映射在 Function<Integer, String> bi
* 参数 整数类型 a 传入，默认 10 ，返回 "Result: " + (a * 2)
*  String result = calc((a) -> "Result: " + (a * 2), 10);
*  System.out.println(result);
*  结果 Result: 20
* */
@FunctionalInterface
public interface Function<T,R>{
    /*
    * Function<Integer,Integer> id = Function.identity();
    * System.out.println(id.apply(3));
    * 结果 3
    * 恒等 传入 整数类型 3，返回整数类型 3
    *
    *
    * 这里是实际处理函数过程的核心逻辑
    * private static List<Double> mapIt(List<Double> numbers, Function<Double, Double> fx) {
    *       List<Double> result = new ArrayList<>();
    *       for (Double number : numbers) {
    *           这里实际做的事情就是恒等转化，类型参照 identity 恒等泛型 ，这里传入 Double ，返回 Double
    *           result.add(fx.apply(number));
    *       }
    *       return result;
    * }
    * 这里是实际调用运用的实例
    *  Function<Double, Double> square = number -> number * number;
    *  Function<Double, Double> half = number -> number * 2;
    *  List<Double> numbers = Arrays.asList(10D, 4D, 12D);
    *  System.out.println(mapIt(numbers, Function.<Double>identity()));
    *  结果 [10.0，4.0，12.0]
    */
    static <T> Function<T,T> identity(){
        return t->t;
    }

    /*
    *  Function<String, Integer> reverse = (s)-> Integer.parseInt(s);
    *  Function<Integer,String> converter = (i)-> Integer.toString(i);
    *  System.out.println(converter.apply(3).length());
    *  System.out.println(converter.compose(reverse).apply("30").length());
    *  结果
    *       1
    *       2
    *  注意 compose 调用者和参数实际处理的顺序
    *  先 reverse.apply("30")
    *  结果 30
    *  在 converter.apply("30")
    *  结果 "30"
    *  最后 "30".length()
    *  结果 2
    */
    default <V> Function<V,R> compose(Function<? super V,? extends T> before){
        Objects.requireNonNull(before);
        return (V v)->apply(before.apply(v));
    }

    /*
    * * Function<Integer,String> converter = (i)-> Integer.toString(i);
    * System.out.println(converter.apply(3).length());
    * System.out.println(converter.apply(30).length());
    * 结果
    *       1
    *       2
    * */
    R apply(T t);

    /*
    *  Function<String, Integer> reverse = (s)-> Integer.parseInt(s);
    *  Function<Integer,String> converter = (i)-> Integer.toString(i);
    *  System.out.println(converter.apply(3).length());
    *  System.out.println(converter.andThen(reverse).apply(30).byteValue());
    *  结果
    *       1
    *       30
    *  注意 compose 调用者和参数实际处理的顺序
    *  先 converter.apply(30)
    *  结果 "30"
    *  在 reverse.apply("30")
    *  结果 30
    *  最后 30.byteValue()
    *  结果 30
    */
    default <V> Function<T,V> andThen(Function<? super R,? extends V> after){
        Objects.requireNonNull(after);
        return (T t)->after.apply(apply(t));
    }
}
