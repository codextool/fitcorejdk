package java.util.function;

import java.util.Objects;

/*
 * Consumer<String> c = (x) -> System.out.println(x.toLowerCase());
 * c.accept("www.topicsky.top");
 * 结果 www.topicsky.top
 *
 *
 * 作为一个消费接口，带入参数计算处理，不需要回馈什么
 * int x = 99;
 * Consumer<Integer> myConsumer = (y) ->
 *     {
 *     System.out.println("x = " + x); // Statement A
 *     System.out.println("y = " + y);
 *     };
 * myConsumer.accept(x);
 * 结果
 *      x=99
 *      y=99
 *
 * 稍微复杂点的例子
 * 原始类
 * class Student {
 *      public String name;
 *      public double gpa;
 *      Student(String name, double g) {
 *          this.name = name;
 *          this.gpa = g;
 *      }
 * }
 * 实际的处理核心
 * public static void acceptAllEmployee(List<Student> student, Consumer<Student> printer) {
 *      for (Student e : student) {
 *          printer.accept(e);
 *      }
 * }
 *
 * 调用使用实例
 *  List<Student> students = Arrays.asList(
 *      new Student("John", 3),
 *      new Student("Mark", 4)
 *  );
 *
 *  acceptAllEmployee(students, e -> System.out.println(e.name));
 *  acceptAllEmployee(students, e -> {
 *      e.gpa *= 1.5;
 *      }
 *  );
 *  acceptAllEmployee(students, e -> System.out.println(e.name + ": " + e.gpa));
 *
 * 处理结果
 *      John
 *      Mark
 *      John：4.5
 *      Mark：6.0
 */

@FunctionalInterface
public interface Consumer<T>{
    /*
     * 基础数据载体
     * class Student {
     *      public int id;
     *      public double gpa;
     *      public String name;
     *
     *      Student(int id, long g, String name) {
     *          this.id = id;
     *          this.gpa = g;
     *          this.name = name;
     *      }
     *
     *      @Override
     *      public String toString() {
     *          return id + ">" + name + ": " + gpa;
     *      }
     * 实际处理逻辑实现
     * private static void raiseStudents(List<Student> employees,Consumer<Student> fx) {
     *      for (Student e : employees) {
     *      fx.accept(e);
     *      }
     * }
     * 这里是应用调用实例
     * List<Student> students = Arrays.asList(
     *      new Student(1, 3, "John"),
     *      new Student(2, 4, "Jane"),
     *      new Student(3, 3,"Jack"));
     *
     *      Consumer<Student> raiser = e -> {
     *          e.gpa = e.gpa * 1.1;
     *      };
     *
     *      raiseStudents(students, System.out::println);
     *      raiseStudents(students, raiser.andThen(System.out::println)
     * );
     * 结果
     * 1> John:3.0
     * 2> Jane:4.0
     * 3> Jack:3.0
     * 1> John:3.3000000000000003
     * 2> Jane:4.4
     * 3> Jack:3.3000000000000003
     * */
    default Consumer<T> andThen(Consumer<? super T> after){
        Objects.requireNonNull(after);
        return (T t)->{
            accept(t);
            after.accept(t);
        };
    }

    /*
     *  List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
     *  关于stream 的number的forEach方法，传一个函数式接口，这里构造覆写，自定义消费规则
     *  numbers.forEach(new Consumer<Integer>() {
     *      @Override
     *      public void accept(Integer integer) {
     *          System.out.println(integer);
     *      }
     *  }
     *  结果
     *  1
     *  2
     *  3
     *  4
     *  5
     *  6
     *  7
     *  8
     *  9
     *  10
     * */
    void accept(T t);
}
