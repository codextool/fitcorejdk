package java.util.function;

@FunctionalInterface
public interface LongSupplier{
    /*
    * LongSupplier i = () -> Long.MAX_VALUE;
    * System.out.println(i.getAsLong());
    * ��� 9223372036854775807
    * */
    long getAsLong();
}
