package java.util.function;

@FunctionalInterface
public interface DoubleFunction<R>{
    /*
    * DoubleFunction<String> df = (d) -> d +"  被转化为字符串类型了";
    * System.out.println(df.apply(0.5));
    * 结果 0.5 被转化为字符串类型了
    * */
    R apply(double value);
}
