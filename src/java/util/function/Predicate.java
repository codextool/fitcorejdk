package java.util.function;

import java.util.Objects;

/*
*  Predicate<String> i  = (s)-> s.length() > 5;
*  System.out.println(i.test("www.topicsky.top "));
*  结果 true
*  将字符串传入判断 bioolean，并返回
*
*  这是一个相对丰富的实例
*  这是数据对象
*  class Box {
*       private int weight = 0;
*       private String color = "";
*       public Box(int weight, String color) {
*           this.weight = weight;
*           this.color = color;
*       }
*       public Integer getWeight() {
*           return weight;
*       }
*       public void setWeight(Integer weight) {
*           this.weight = weight;
*       }
*       public String getColor() {
*           return color;
*       }
*       public void setColor(String color) {
*           this.color = color;
*       }
*       public String toString() {
*           return "Apple{" + "color='" + color + '\'' + ", weight=" + weight + '}';
*       }
* }
* 这是函数过程的实际逻辑体
* public static boolean isGreenApple(Box apple) {
*       return "green".equals(apple.getColor());
* }
*
* public static boolean isHeavyApple(Box apple) {
*       return apple.getWeight() > 150;
* }
*
* 这里自定义了过滤器规则
* public static List<Box> filter(List<Box> inventory,Predicate<Box> p) {
* 这里通过对不同函数过程的调用实现不同功能筛选
*   List<Box> result = new ArrayList<>();
*   for (Box apple : inventory) {
*       if (p.test(apple)) {
*           result.add(apple);
*       }
*   }
*   return result;
* }
*
* 这里是实际调用运用实例
* 创建源数据
* List<Box> inventory = Arrays.asList(new Box(80, "green"), new Box(155, "green"), new Box(120, "red"));
* 通过 isGreenApple 筛选
* List<Box> greenApples = filter(inventory,Main::isGreenApple);
* System.out.println(greenApples);
* 通过 isGreenApple 筛选
* List<Box> heavyApples = filter(inventory,Main::isHeavyApple);
* System.out.println(heavyApples);
* 通过 getColor 筛选
* List<Box> greenApples2 = filter(inventory,(Box a) -> "green".equals(a.getColor()));
* System.out.println(greenApples2);
* 通过 getWeight 筛选
* List<Box> heavyApples2 = filter(inventory,(Box a) -> a.getWeight() > 150);
* System.out.println(heavyApples2);
* 通过 getWeight 和 getColor 筛选
* List<Box> weirdApples = filter(inventory,(Box a) -> a.getWeight() < 80 || "brown".equals(a.getColor()));
* System.out.println(weirdApples);
*
* 结果
*
* [Apple{color='green', weight=80}, Apple{color='green', weight=155}]
* [Apple{color='green', weight=155}]
* [Apple{color='green', weight=80}, Apple{color='green', weight=155}]
* [Apple{color='green', weight=155}]
* []
*
*
* 还有一个实例，比较浅显
* 基础数据对象
* class Student {
*    public int id;
*    public long gpa;
*    public String name;
*    Student(int id, long g, String name) {
*       this.id = id;
*       this.gpa = g;
*       this.name = name;
*    }
*    @Override
*    public String toString() {
*       return id + ">" + name + ": " + gpa;
*    }
* }
* 这是函数核心处理逻辑
* 这个在这里是第一次处理函数过程，被包含在大逻辑中，预先处理
*  private static Predicate<Student> createCustomPredicateWith(double threshold) {
*    return e -> e.gpa > threshold;
* }
*
* 这里是大逻辑处理的逻辑过程
* private static List<Student> findStudents(List<Student> employees, Predicate<Student> condition) {
*    List<Student> result = new ArrayList<>();
*    for (Student e : employees) {
*       if (condition.test(e)) {
*          result.add(e);
*       }
*    }
*    return result;
* }
* 这是实际调用过程
* List<Student> employees = Arrays.asList(
*       new Student(1, 3, "John"),
*       new Student(2, 3, "Jane"),
*       new Student(3, 4, "Jack")
* );
*
* // with predicate
* 这里的小函数过程返回 Predicate 接口类型，然后再次调用，结果复用
* System.out.println(findStudents(employees, createCustomPredicateWith(10_000)));
*
* // with function definition, both are same
* 这里的效果类似，Function 的处理结果 也是 Predicate 返回，然后调用另一个函数过程，结果复用
* Function<Double, Predicate<Student>> customFunction = threshold -> (e -> e.gpa > threshold);
* System.out.println(findStudents(employees, customFunction.apply(10_000D)));
*
* 结果
*       []
*       []
*  */
@FunctionalInterface
public interface Predicate<T>{
    /*
    *  Predicate<String> i  = Predicate.isEqual("www.baidu.com");
    *  System.out.println(i.test("www.topicsky.top"));
    *  结果 false
    *  是否值相等，这里字符串值不相等
    */
    static <T> Predicate<T> isEqual(Object targetRef){
        return (null==targetRef)
                ?Objects::isNull
                :object->targetRef.equals(object);
    }

    /*
    *  Predicate<String> i  = (s)-> s.length() > 5;
    *  Predicate<String> j  = (s)-> s.length() < 3;
    *  System.out.println(i.and(j).test("www.topicsky.top "));
    *  结果 false
    *  i true ，j false
    *  全局全部 true 失败
    *  最终结果 false
    */
    default Predicate<T> and(Predicate<? super T> other){
        Objects.requireNonNull(other);
        return (t)->test(t)&&other.test(t);
    }

    /*
    *  Predicate<String> i  = (s)-> s.length() > 5;
    *  System.out.println(i.test("www.topicsky.top "));
    *  结果 true
    *  将字符串传入判断 bioolean，并返回
    * */
    boolean test(T t);

    /*
    * Predicate<String> i  = (s)-> s.length() > 5;
    * System.out.println(i.negate().test("www.topicsky.top "));
    * 结果 false
    * 函数过程为 true
    * 取反为false
    * */
    default Predicate<T> negate(){
        return (t)->!test(t);
    }

    /*
    * Predicate<String> i  = (s)-> s.length() > 5;
    * Predicate<String> j  = (s)-> s.length() < 3;
    * System.out.println(i.or(j).test("java2s.com "));
    * 结果 true
    *  i true ，j false
    *  全局其中任意一个 true 成功
    *  最终结果 true
    */
    default Predicate<T> or(Predicate<? super T> other){
        Objects.requireNonNull(other);
        return (t)->test(t)||other.test(t);
    }
}
