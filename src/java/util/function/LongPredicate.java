package java.util.function;

import java.util.Objects;

/*
* LongPredicate i = (l) -> l>0;
* System.out.println(i.test(Long.MAX_VALUE));
* 结果 true
* */
@FunctionalInterface
public interface LongPredicate{
    /*
    *  LongPredicate i = (l) -> l > 0;
    *  LongPredicate j = (l) -> l == 0;
    *  System.out.println(i.and(j).test(Long.MAX_VALUE));
    *  结果 true
    *  同时满足 i true ， j true
    *  最终成立 true
    *  */
    default LongPredicate and(LongPredicate other){
        Objects.requireNonNull(other);
        return (value)->test(value)&&other.test(value);
    }

    /*
    * LongPredicate i = (l) -> l>0;
    * System.out.println(i.test(Long.MAX_VALUE));
    * 结果 true
    * */
    boolean test(long value);

    /*
    *  LongPredicate i = (l) -> l > 0;
    *  System.out.println(i.negate().test(Long.MAX_VALUE));
    *  结果 false
    *  将原本结果 取反 ，原本 true
    *  最终结果 不成立 false
    *  */
    default LongPredicate negate(){
        return (value)->!test(value);
    }

    /*
    *  LongPredicate i = (l) -> l > 0;
    *  LongPredicate j = (l) -> l == 0;
    *  System.out.println(i.or(j).test(Long.MAX_VALUE));
    *  结果 true
    *  只要有其中一个 函数逻辑过程成立则全体成立
    *  i true ，j true
    *  最终结果 true
    */
    default LongPredicate or(LongPredicate other){
        Objects.requireNonNull(other);
        return (value)->test(value)||other.test(value);
    }
}
