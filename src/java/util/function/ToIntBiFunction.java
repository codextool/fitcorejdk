package java.util.function;

@FunctionalInterface
public interface ToIntBiFunction<T,U>{
    /*
    * ToIntBiFunction<String,String> i  = (x,y)-> Integer.parseInt(x) +Integer.parseInt(x);
    * System.out.println(i.applyAsInt("2","3"));
    * 结果 4
    * 函数处理两个 int 参数 封装到 int 并返回
    * */
    int applyAsInt(T t,U u);
}
