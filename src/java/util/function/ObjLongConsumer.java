/**
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package java.util.function;

@FunctionalInterface
public interface ObjLongConsumer<T>{
    /*
    *  ObjLongConsumer<String> i  = (s,d)->System.out.println(s+d);
    *  i.accept("www.topicsky.top ",Long.MAX_VALUE);
    *  结果 www.topicsky.top 9223372036854775807
    *  同 ObjDoubleConsumer
    *  这里是对消费接口的补充，参数未限制，但是类型相对固定，无返回
    *  */
    void accept(T t,long value);
}
