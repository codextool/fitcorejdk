package java.util.function;

@FunctionalInterface
public interface ToLongBiFunction<T,U>{
    /*
    *  ToLongBiFunction<String,String> i  = (x,y)-> Long.parseLong(x)+Long.parseLong(y);
    *  System.out.println(i.applyAsLong("2","2"));
    *  结果 4 作为双参数函数接口，函数过程处理封装并返回 Long
    *  */
    long applyAsLong(T t,U u);
}
