package java.util.function;

@FunctionalInterface
public interface ObjIntConsumer<T>{
    /*
      *  ObjIntConsumer<String> i  = (s,d)->System.out.println(s+d);
      *  i.accept("www.topicsky.top",0.1234);
      *  结果 www.topicsky.top 0.1234
      *  同 ObjDoubleConsumer 类似
      *  这里是对消费接口的补充，参数未限制，但是类型相对固定，无返回
      *  */
    void accept(T t,int value);
}
