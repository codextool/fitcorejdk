package java.util.function;

/*
 * DoubleBinaryOperator d = (x,y) -> x*y;
 * System.out.println(d.applyAsDouble(0.23, 0.45));
 * ��� 0.103500000000000001
 */
@FunctionalInterface
public interface DoubleBinaryOperator{
    double applyAsDouble(double left,double right);
}
