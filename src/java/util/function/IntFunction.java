package java.util.function;

@FunctionalInterface
public interface IntFunction<R>{
    /*
    * IntFunction<String> i = (x)->Integer.toString(x);
    * System.out.println(i.apply(3).length());
    * 结果 1
    * 先 i.apply(3)
    * 结果 "3"
    * 然后 "3".length()
    * 结果 1
    * */
    R apply(int value);
}
