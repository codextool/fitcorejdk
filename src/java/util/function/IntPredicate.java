package java.util.function;

import java.util.Objects;

/*
* IntPredicate i = (x)-> x < 0;
* System.out.println(i.test(123));
* 结果 false
* */
@FunctionalInterface
public interface IntPredicate{
    /*
    *  IntPredicate i = (x)-> x < 0;
    *  IntPredicate j = (x)-> x == 0;
    *  System.out.println(i.and(j).test(123));
    *  结果 false
    *  同时满足两种 函数逻辑结果 i 和 j 过程
    *  i false ，j false
    *  最终结果 false
    * */
    default IntPredicate and(IntPredicate other){
        Objects.requireNonNull(other);
        return (value)->test(value)&&other.test(value);
    }

    /*
    * IntPredicate i = (x)-> x < 0;
    * System.out.println(i.test(123));
    * 结果 false
    * */
    boolean test(int value);

    /*
    * IntPredicate i = (x)-> x < 0;
    * System.out.println(i.negate().test(123));
    * 这里将 函数过程结果取反
    * i false
    * 取反 结果为 true
    * */
    default IntPredicate negate(){
        return (value)->!test(value);
    }

    /*
    *  IntPredicate i = (x)-> x < 0;
    *  IntPredicate j = (x)-> x == 0;
    *  System.out.println(i.or(j).test(123));
    *  只要满足其中一个过程为 true 则 全体true成立
    *  这里 i false ， j false
    *  最终结果 全部 false ， 则失败 false
    * */
    default IntPredicate or(IntPredicate other){
        Objects.requireNonNull(other);
        return (value)->test(value)||other.test(value);
    }
}
