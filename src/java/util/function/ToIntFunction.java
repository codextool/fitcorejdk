package java.util.function;

@FunctionalInterface
public interface ToIntFunction<T>{
    /*
    * ToIntFunction<String> i  = (x)-> Integer.parseInt(x);
    * System.out.println(i.applyAsInt("2"));
    * 结果 2
    * 作为唯一参数函数接口，处理逻辑结束封装并返回 int
    * */
    int applyAsInt(T value);
}
