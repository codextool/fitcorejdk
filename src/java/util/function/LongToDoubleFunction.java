package java.util.function;

@FunctionalInterface
public interface LongToDoubleFunction{
    /*
    * LongToDoubleFunction i = (l) -> Math.sin(l);
    * System.out.println(i.applyAsDouble(Long.MAX_VALUE));
    * 结果 0.9999303766734422
    * 将处理结果封装到 Double 并返回
    * */
    double applyAsDouble(long value);
}
