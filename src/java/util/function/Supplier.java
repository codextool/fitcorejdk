package java.util.function;

@FunctionalInterface
public interface Supplier<T>{
    /*
    * Supplier<String> i  = ()-> "www.topicsky.top";
    * System.out.println(i.get());
    * 结果 www.topicsky.top
    * 这是供应者，他提供一个字符串 www.topicsky.top
    *
    * 这个实例提供一个参数引用
    * 这是基础引用类
    * class SunPower {
    *       public SunPower() {
    *           System.out.println("Sun Power 初始化..");
    *       }
    * }
    * 实例处理
    * public static SunPower produce(Supplier<SunPower> supp) {
    *     return supp.get();
    * }
    * public static void main(String[] args) {
    *     SunPower power = new SunPower();
    *     SunPower p1 = produce(() -> power);
    *     SunPower p2 = produce(() -> power);
    *     System.out.println("是否在重复同一个对象? " + Objects.equals(p1, p2));
    * }
    * 结果
    *       Sun Power 初始化
    *       是否在重复同一个对象?  true
    *
    * 接下来这个实例运用构造作为方法引用
    * 基础类对象
    * class Employee {
    *  @Override
    *  public String toString() {
    *    return "一个普通员工";
    *  }
    *}
    * 实际调用
    *  public static void main(String[] args) {
    *   直接调用构造 new 作为函数参数
    *    System.out.println(maker(Employee::new));
    *  }
    *
    *  private static Employee maker(Supplier<Employee> fx) {
    *    return fx.get();
    *  }
    *
    *  这个实例指定用户定义函数和方法引用
    *  基础类
    *  class Student {
    *     public String name;
    *     public double gpa;
    *     Student(String name, double g) {
    *       this.name = name;
    *       this.gpa = g;
    *     }
    *     @Override
    *     public String toString() {
    *       return name + ": " + gpa;
    *     }
    *  }
    *  实际调用
    *  Supplier<Student> studentGenerator = Main::employeeMaker;
    *  for (int i = 0; i < 10; i++) {
    *    System.out.println("#" + i + ": " + studentGenerator.get());
    *  }
    *
    *  public static Student employeeMaker() {
    *     return new Student("A",2);
    *  }
    *  结果
    * #0: A: 2.0
    * #1: A: 2.0
    * #2: A: 2.0
    * #3: A: 2.0
    * #4: A: 2.0
    * #5: A: 2.0
    * #6: A: 2.0
    * #7: A: 2.0
    * #8: A: 2.0
    * #9: A: 2.0
    * */
    T get();
}
