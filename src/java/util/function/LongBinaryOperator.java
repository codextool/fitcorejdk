package java.util.function;

@FunctionalInterface
public interface LongBinaryOperator{
    /*
    *
    * LongBinaryOperator i = (x,y) -> x/y;
    * System.out.println(i.applyAsLong(Long.MAX_VALUE,Long.MAX_VALUE));
    * 结果 1
    * 将参数进行定义的过程函数逻辑处理，并返回对应结果
    */
    long applyAsLong(long left,long right);
}
