package java.util.function;

@FunctionalInterface
public interface LongToIntFunction{
    /*
    *  LongToIntFunction i = (l) -> (int)l;
    *  System.out.println(i.applyAsInt(Long.MAX_VALUE));
    *  结果 -1
    *  应为 long 的有效值比 int 大的多，强转过程中就丢失了
    *  */
    int applyAsInt(long value);
}
