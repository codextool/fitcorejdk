package java.util.function;

import java.util.Objects;

/*
*  LongUnaryOperator i = (l) -> -l;
*  System.out.println(i.applyAsLong(Long.MAX_VALUE));
*  结果 -9223372036854775807
*  */
@FunctionalInterface
public interface LongUnaryOperator{
    /*
    * LongUnaryOperator i = LongUnaryOperator.identity();
    * System.out.println(i.applyAsLong(Long.MAX_VALUE));
    * 结果 9223372036854775807
    * 恒等过继值封装到 Long ，并返回
    * */
    static LongUnaryOperator identity(){
        return t->t;
    }

    /*
    *  LongUnaryOperator i = (l) -> -l;
    *  LongUnaryOperator j = (l) -> l/2;
    *  System.out.println(i.compose(j).applyAsLong(Long.MAX_VALUE));
    *  结果 -4611686018427387903
    *  先处理 compose 参数 过程逻辑
    *  然后处理 compose 调用者过程逻辑
    *  统一封装结果到 Long 返回相应结果
    *  */
    default LongUnaryOperator compose(LongUnaryOperator before){
        Objects.requireNonNull(before);
        return (long v)->applyAsLong(before.applyAsLong(v));
    }

    /*
    *  LongUnaryOperator i = (l) -> -l;
    *  System.out.println(i.applyAsLong(Long.MAX_VALUE));
    *  结果 -9223372036854775807
    *  */
    long applyAsLong(long operand);

    /*
    *  LongUnaryOperator i = (l) -> -l;
    *  LongUnaryOperator j = (l) -> l/2;
    *  System.out.println(i.andThen(j).applyAsLong(Long.MAX_VALUE));
    *  结果 -4611686018427387903
    *  先处理 andThen 调用者 过程逻辑
    *  然后处理 andThen 参数 过程逻辑
    *  统一封装结果到 Long 返回相应结果
    *  */
    default LongUnaryOperator andThen(LongUnaryOperator after){
        Objects.requireNonNull(after);
        return (long t)->after.applyAsLong(applyAsLong(t));
    }
}
