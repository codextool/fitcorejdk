package java.util.function;

import java.util.Objects;

/*
* IntConsumer ic = (x)->System.out.println(x);
* ic.accept(3);
* 结果 3
* */
@FunctionalInterface
public interface IntConsumer{
    /*
    * 这里的默认实现 实现轮调，参数推进，组合函数
    * IntConsumer ic = (x)->System.out.println(x);
    * ic.andThen(ic).accept(3);
    * 结果
    *       3
    *       3
    * 先执行 andThen 调用者的执行过程，然后执行参数过程
    * */
    default IntConsumer andThen(IntConsumer after){
        /*
        * 例行判断是否存在
        * */
        Objects.requireNonNull(after);
        /*
        * 这里需要注意的是，调用 andThen 的实例 t 参数来源
        * 首先 本身是 IntConsumer 被调用参数，也就是 this
        * 这是组合函数
        * */
        return (int t)->{
            //this.accept(t);
            accept(t);
            after.accept(t);
        };
    }
    /*
     * IntConsumer ic = (x)->System.out.println(x);
     * ic.accept(3);
     * 结果 3
     *
     * */
    void accept(int value);
}
