package java.util.function;

@FunctionalInterface
public interface ToDoubleFunction<T>{
    /*
    * ToDoubleFunction<Integer> i  = (x)-> Math.sin(x);
    * System.out.println(i.applyAsDouble(Integer.MAX_VALUE));
    * 结果 -0.7249165551445564
    * 参数处理结束 封装到 Double 并返回
    * */
    double applyAsDouble(T value);
}
