/**
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package java.util.function;

@FunctionalInterface
public interface ToLongFunction<T>{
    /*
    *  ToLongFunction<String> i  = (x)-> Long.parseLong(x);
    *  System.out.println(i.applyAsLong("2"));
    *  结果 2 作为唯一参数函数接口，函数过程处理封装并返回 Long
    *  */
    long applyAsLong(T value);
}
