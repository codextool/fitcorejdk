package java.util.function;

import java.util.Objects;

/*
*  DoubleConsumer d = (x) -> System.out.println(x*x);
*  d.accept(0.23);
*  ��� 0.0529
* */
@FunctionalInterface
public interface DoubleConsumer{
    /*
    * DoubleConsumer d = (x) -> System.out.println(x*x);
    * d.andThen(d).andThen(d).accept(0.23);
    * ���
    * 0.0529
    * 0.0529
    * 0.0529
    * */
    default DoubleConsumer andThen(DoubleConsumer after){
        Objects.requireNonNull(after);
        return (double t)->{
            accept(t);
            after.accept(t);
        };
    }

    void accept(double value);
}
