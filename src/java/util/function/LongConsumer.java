package java.util.function;

import java.util.Objects;

/*
* LongConsumer i = (l) -> System.out.println(l);
* i.accept(Long.MAX_VALUE);
* 结果 9223372036854775807
* 作为消费接口，给你就吃，不求回报
* */
@FunctionalInterface
public interface LongConsumer{
    /*
    *  LongConsumer i = (l) -> System.out.println(l);
    *  i.andThen(i).accept(Long.MAX_VALUE);
    * 结果 9223372036854775807
    * 作为消费接口，给你就吃，不求回报
    * 当然还要注意 andThen 方法的先后，调用者先执行，然后是参数逻辑
    */
    default LongConsumer andThen(LongConsumer after){
        Objects.requireNonNull(after);
        return (long t)->{
            accept(t);
            after.accept(t);
        };
    }

    /*
    * LongConsumer i = (l) -> System.out.println(l);
    * i.accept(Long.MAX_VALUE);
    * 结果 9223372036854775807
    * 作为消费接口，给你就吃，不求回报
    * */
    void accept(long value);
}
