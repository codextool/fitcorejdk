package java.util.function;

import java.util.Objects;

/*
* IntUnaryOperator i = (x) -> x*x;
* System.out.println(i.applyAsInt(2));
* 结果（参数运算完毕 封装到 int 并返回）
* 4
* */
@FunctionalInterface
public interface IntUnaryOperator{
    /*
    * IntUnaryOperator i = IntUnaryOperator.identity();
    * System.out.println(i.compose(i).applyAsInt(2));
    * 结果 2
    * 首先执行 compose 方法的参数过程
    * 在执行 compose的调用方过程
    * 过程是恒等过继
    * 这两个过程统一将执行运算结果封装到 int 并返回
    */
    static IntUnaryOperator identity(){
        return t->t;
    }

    /*
    * IntUnaryOperator i = (x) -> x*x;
    * System.out.println(i.compose(i).applyAsInt(2));
    * 结果 16
    * 首先执行 compose 方法的参数过程
    * 在执行 compose的调用方过程
    * 这两个过程统一将执行运算结果封装到 int 并返回
    */
    default IntUnaryOperator compose(IntUnaryOperator before){
        Objects.requireNonNull(before);
        return (int v)->applyAsInt(before.applyAsInt(v));
    }

    /*
    * IntUnaryOperator i = (x) -> x*x;
    * System.out.println(i.applyAsInt(2));
    * 结果（参数运算完毕 封装到 int 并返回）
    * 4
    * */
    int applyAsInt(int operand);

    /*
    * IntUnaryOperator i = (x) -> x*x;
    * System.out.println(i.andThen(i).applyAsInt(2));
    * 结果 16
    * 首先执行 andThen 的调用方过程
    * 在执行 andThen 方法的参数过程
    * 这两个过程统一将执行运算结果封装到 int 并返回
    */
    default IntUnaryOperator andThen(IntUnaryOperator after){
        Objects.requireNonNull(after);
        return (int t)->after.applyAsInt(applyAsInt(t));
    }
}
