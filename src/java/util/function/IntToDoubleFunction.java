package java.util.function;

@FunctionalInterface
public interface IntToDoubleFunction{
    /*
    *  IntToDoubleFunction i = (x) -> {return Math.sin(x);};
    *  System.out.println(i.applyAsDouble(2));
    *  结果 （这里去的是参数的 sin 值，并送到 Double 封装）
    *  0.9092974268256817
    *  */
    double applyAsDouble(int value);
}
